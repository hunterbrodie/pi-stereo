from subprocess import Popen, PIPE
import board
import digitalio
import adafruit_character_lcd.character_lcd as characterlcd
from os.path import exists

# Modify this if you have a different sized character LCD
lcd_columns = 16
lcd_rows = 2

# compatible with all versions of RPI as of Jan. 2019
# v1 - v3B+
lcd_rs = digitalio.DigitalInOut(board.D22)
lcd_en = digitalio.DigitalInOut(board.D17)
lcd_d4 = digitalio.DigitalInOut(board.D25)
lcd_d5 = digitalio.DigitalInOut(board.D24)
lcd_d6 = digitalio.DigitalInOut(board.D23)
lcd_d7 = digitalio.DigitalInOut(board.D15)

def get_music():
    return run_cmd("playerctl --player spotifyd metadata --format '{{title}}\n{{artist}}'")

# run unix shell command, return as ASCII
def run_cmd(cmd):
    p = Popen(cmd, shell=True, stdout=PIPE)
    output = p.communicate()[0]
    return output.decode('ascii')

message = get_music()

change = True

if (exists('/home/pi/message.txt')):
    f = open("/home/pi/message.txt", "r")
    data = f.read()
    f.close()

    change = (message != data)

if (change):
    # Initialise the lcd class
    lcd = characterlcd.Character_LCD_Mono(lcd_rs, lcd_en, lcd_d4, lcd_d5, lcd_d6, lcd_d7, lcd_columns, lcd_rows)

    # wipe LCD screen before we start
    lcd.clear()

    # display music info
    lcd.message = message

    with open('/home/pi/message.txt', "w") as f:
        f.write(message)

