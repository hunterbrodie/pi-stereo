from gpiozero import LED, Button, DigitalOutputDevice
import os
from os.path import exists
import socket
import board
import digitalio
import adafruit_character_lcd.character_lcd as characterlcd
import time
from datetime import datetime, timedelta
from signal import pause

Button.pressed_time = None
Button.power_state = True

button = Button(pin = 4, hold_time = 0.3)
led = LED(14)
transistor = DigitalOutputDevice(5)

# Modify this if you have a different sized character LCD
lcd_columns = 16
lcd_rows = 2

# compatible with all versions of RPI as of Jan. 2019
# v1 - v3B+
lcd_rs = digitalio.DigitalInOut(board.D22)
lcd_en = digitalio.DigitalInOut(board.D17)
lcd_d4 = digitalio.DigitalInOut(board.D25)
lcd_d5 = digitalio.DigitalInOut(board.D24)
lcd_d6 = digitalio.DigitalInOut(board.D23)
lcd_d7 = digitalio.DigitalInOut(board.D15)

lcd = characterlcd.Character_LCD_Mono(lcd_rs, lcd_en, lcd_d4, lcd_d5, lcd_d6, lcd_d7, lcd_columns, lcd_rows)

transistor.on()
led.on()

def toggle_power(state):
    if (state):
        transistor.off()
        led.off()
        lcd.clear()
        os.system("systemctl --user stop spotifyd.service")
    else:
        transistor.on()
        led.on()
        os.system("systemctl --user start spotifyd.service")

    return not state

def print_ip():
    s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
    try:
        # doesn't even have to be reachable
        s.connect(('10.255.255.255', 1))
        ip = s.getsockname()[0]
    except Exception:
        ip = '127.0.0.1'
    finally:
        s.close()

    lcd.clear()
    lcd.message = ip

def old_message():
    if (exists('/home/pi/message.txt')):
        f = open("/home/pi/message.txt", "r")
        data = f.read()
        f.close()
        return data
    return None

def start_timer(btn):
    btn.pressed_time = datetime.now()

def power_ip_check(btn):
    if (btn.pressed_time + timedelta(seconds = 0.3) > datetime.now()):
        btn.power_state = toggle_power(btn.power_state)
    else:
        lcd.clear()
        message = old_message()
        if (message != None):
            lcd.message = message

button.when_pressed = start_timer
button.when_released = power_ip_check
button.when_held = print_ip

pause()
