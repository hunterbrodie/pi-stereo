from gpiozero import RotaryEncoder, Button
import os
from datetime import datetime, timedelta
import time
import board
import digitalio
import adafruit_character_lcd.character_lcd as characterlcd
from os.path import exists

RotaryEncoder.time_passed = None

# Modify this if you have a different sized character LCD
lcd_columns = 16
lcd_rows = 2

# compatible with all versions of RPI as of Jan. 2019
# v1 - v3B+
lcd_rs = digitalio.DigitalInOut(board.D22)
lcd_en = digitalio.DigitalInOut(board.D17)
lcd_d4 = digitalio.DigitalInOut(board.D25)
lcd_d5 = digitalio.DigitalInOut(board.D24)
lcd_d6 = digitalio.DigitalInOut(board.D23)
lcd_d7 = digitalio.DigitalInOut(board.D15)

lcd = characterlcd.Character_LCD_Mono(lcd_rs, lcd_en, lcd_d4, lcd_d5, lcd_d6, lcd_d7, lcd_columns, lcd_rows)

rotor = RotaryEncoder(9, 10, None, 10)
button = Button(11)

left_end = "-["
right_end = "]+"
volume_string = "------------"
filler_string = "            "

def change_volume(rtr):
    volume = int(rotor.value * 50 + 50)
    command = "pactl set-sink-volume @DEFAULT_SINK@ " + str(volume) + "%"
    os.system(command)

    volume_display = int(rotor.value * 6 + 6)
    message = left_end + volume_string[0:volume_display] + filler_string[volume_display:12] + right_end + "\nvolume"
    lcd.clear()
    lcd.message = message

    rtr.time_passed = datetime.now()

def mute():
    os.system("pactl set-sink-mute @DEFAULT_SINK@ toggle")

def reset_lcd():
    data = None
    if (exists('/home/pi/message.txt')):
        f = open("/home/pi/message.txt", "r")
        data = f.read()
        f.close()
    
    lcd.clear()
    if (data != None):
        lcd.message = data
      

rotor.when_rotated = change_volume
button.when_pressed = mute

while True:
    if (rotor.time_passed != None and rotor.time_passed + timedelta(seconds = 1) < datetime.now()):
        reset_lcd()
        rotor.time_passed = None
    time.sleep(0.1)
